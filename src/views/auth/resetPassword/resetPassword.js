import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";

// Chakra imports
import {
  Box,
  Button,
  Checkbox,
  Flex,
  FormControl,
  FormLabel,
  Heading,
  Icon,
  Input,
  InputGroup,
  InputRightElement,
  Link,
  SimpleGrid,
  Text,
  useColorModeValue,
} from "@chakra-ui/react";
import { MdOutlineRemoveRedEye } from "react-icons/md";
import { RiEyeCloseLine } from "react-icons/ri";

// Custom components
import DefaultAuth from "layouts/auth/types/Default";

// Assets
import illustration from "assets/img/auth/auth.png";
import { useResetPassword } from "hooks/useResetPassword";
import { useAuthContext } from "hooks/useAuthContext";
import { Spinner } from "components/spinner";

function ResetPassword() {
  // Chakra color mode
  const textColor = useColorModeValue("navy.700", "white");
  const textColorSecondary = "gray.400";
  const brandStars = useColorModeValue("brand.500", "brand.400");
  const [show, setShow] = useState(false);
  const [currentUser, setCurrentUser] = useState(undefined);
  const [password, setPassword] = useState("");
  const [password2, setPassword2] = useState("");
  const [successMsg, setSuccessMsg] = useState("");
  const [errorMsg, setErrorMsg] = useState("");
  const [inProgress, setInProgress] = useState(false);
  const { recoverPassword, isLoading, error } = useResetPassword();
  const { user } = useAuthContext();
  const history = useHistory();

  const handleClick = () => setShow(!show);
  const handleUpdatePassword = async () => {
    setErrorMsg(null);
    if (!currentUser?.id) {
      setErrorMsg("Can't get user id");
      return;
    }
    if (password !== password2) {
      setErrorMsg("Password dosen't match");
      return;
    }
    setInProgress(true);
    const response = await fetch(
      process.env.REACT_APP_API_URL + "/users/" + currentUser?.id,
      {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: user,
        },
        body: JSON.stringify({
          user: {
            password: password,
          },
        }),
      }
    );
    const json = await response.json();

    if (!response.ok) {
      setInProgress(false);
      setErrorMsg(json.error.message);
    }
    if (response.ok) {
      console.log("UPDATE RES: ", json);
      // save the user to local storage
      setInProgress(false);
      history.push("/projects");
    }
  };

  const handleResetPassword = async () => {
    let url_string = window.location.href;
    let splitted = url_string.split("token=");
    let res = await recoverPassword(splitted[1]);
    if (res) setCurrentUser(res.user);
  };

  useEffect(() => {
    handleResetPassword();
  }, []);

  return (
    <DefaultAuth illustrationBackground={illustration} image={illustration}>
      <Flex
        w="100%"
        maxW="max-content"
        mx={{ base: "auto", lg: "0px" }}
        me="auto"
        h="100%"
        alignItems="start"
        justifyContent="center"
        mb={{ base: "30px", md: "60px", lg: "100px", xl: "60px" }}
        px={{ base: "25px", md: "0px" }}
        mt={{ base: "40px", lg: "16vh", xl: "22vh" }}
        flexDirection="column"
      >
        {error && (
          <Box me="auto" mb="34px" textAlign="center">
            <Heading
              color={textColor}
              fontSize={{ base: "3xl", md: "36px" }}
              mb="16px"
            >
              {error}
            </Heading>
          </Box>
        )}
        {!error && !isLoading && (
          <>
            <Box me="auto" mb="34px">
              <Heading
                color={textColor}
                fontSize={{ base: "3xl", md: "36px" }}
                mb="16px"
              >
                Reset your password
              </Heading>
            </Box>
            <Flex
              zIndex="2"
              direction="column"
              w={{ base: "100%", lg: "456px" }}
              maxW="100%"
              background="transparent"
              borderRadius="15px"
              mx={{ base: "auto", lg: "unset" }}
              me="auto"
              mb={{ base: "20px", md: "auto" }}
              align="start"
            >
              <FormControl>
                <FormLabel
                  ms="4px"
                  fontSize="sm"
                  fontWeight="500"
                  isRequired={true}
                  color={textColor}
                  display="flex"
                >
                  Password<Text color={brandStars}>*</Text>
                </FormLabel>
                <InputGroup size="md">
                  <Input
                    isRequired={true}
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                    variant="auth"
                    fontSize="sm"
                    ms={{ base: "0px", md: "4px" }}
                    placeholder="Min. 6 characters"
                    mb="24px"
                    size="lg"
                    type={show ? "text" : "password"}
                  />
                  <InputRightElement
                    display="flex"
                    alignItems="center"
                    mt="4px"
                  >
                    <Icon
                      color={textColorSecondary}
                      _hover={{ cursor: "pointer" }}
                      as={show ? RiEyeCloseLine : MdOutlineRemoveRedEye}
                      onClick={handleClick}
                    />
                  </InputRightElement>
                </InputGroup>
                <FormLabel
                  ms="4px"
                  fontSize="sm"
                  fontWeight="500"
                  isRequired={true}
                  color={textColor}
                  display="flex"
                >
                  Repeat Password<Text color={brandStars}>*</Text>
                </FormLabel>
                <InputGroup size="md">
                  <Input
                    isRequired={true}
                    value={password2}
                    onChange={(e) => setPassword2(e.target.value)}
                    variant="auth"
                    fontSize="sm"
                    ms={{ base: "0px", md: "4px" }}
                    placeholder="Min. 6 characters"
                    mb="24px"
                    size="lg"
                    type={show ? "text" : "password"}
                  />
                  <InputRightElement
                    display="flex"
                    alignItems="center"
                    mt="4px"
                  >
                    <Icon
                      color={textColorSecondary}
                      _hover={{ cursor: "pointer" }}
                      as={show ? RiEyeCloseLine : MdOutlineRemoveRedEye}
                      onClick={handleClick}
                    />
                  </InputRightElement>
                </InputGroup>
                <Flex mb="10px" alignItems="center" justifyContent={"center"}>
                  <Text color="green">{successMsg}</Text>
                  <Text color="red">{errorMsg}</Text>
                </Flex>
                {inProgress ? (
                  <Spinner />
                ) : (
                  <Button
                    onClick={handleUpdatePassword}
                    fontSize="sm"
                    variant="brand"
                    fontWeight="500"
                    w="100%"
                    h="50"
                    mb="24px"
                  >
                    Save new password
                  </Button>
                )}
              </FormControl>
            </Flex>
          </>
        )}
      </Flex>
    </DefaultAuth>
  );
}

export default ResetPassword;
