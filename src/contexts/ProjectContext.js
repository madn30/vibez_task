import { createContext, useReducer, useEffect } from "react";

export const ProjectContext = createContext();

export const projectReducer = (state, action) => {
    switch (action.type) {
        case "LOGIN":
            return { project: action.payload };
        case "LOGOUT":
            return { project: null };
        default:
            return state;
    }
};

export const ProjectContextProvider = ({ children }) => {
    const [state, dispatch] = useReducer(projectReducer, {
        user: null,
    });

    useEffect(() => {
        const project = JSON.parse(localStorage.getItem("project"));
        console.log("CHECK PROJECT: ", project);

        if (project) {
            dispatch({ type: "LOGIN", payload: project });
        }
    }, []);

    console.log("PROJECT STATE: ", state);

    return (
        <ProjectContext.Provider value={{ ...state, dispatch }}>
            {children}
        </ProjectContext.Provider>
    );
};
